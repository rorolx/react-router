import React from 'react'
// import { Switch, Route } from 'react-router-dom'
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Home from './pages/home.js'
import Page2 from './pages/page2.js'
import Page3 from './pages/page3.js'


class Router extends React.Component {

    render(){
        return(
            <BrowserRouter>
                <Switch>
                    <Route exact path='/' component={Home} />
                    <Route exact path='/page2' component={Page2} />
                    <Route exact path='/page3' component={Page3} />
                </Switch>
            </BrowserRouter>
        )
    }
}

export default Router