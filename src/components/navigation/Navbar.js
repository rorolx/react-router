import React from 'react'
import { NavLink } from 'react-router-dom'

const Navbar = () => <div>
       <nav>
               <NavLink exact to={'/'}>Home</NavLink>
               &nbsp;|&nbsp;
               <NavLink exact to={'/page2'}>Page2</NavLink>
               &nbsp;|&nbsp;
               <NavLink exact to={'/page3'}>Page3</NavLink>
          </nav>
</div>

export default Navbar